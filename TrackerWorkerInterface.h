/*Definition of the general interface for the worker thread of a plugin*/

#ifndef TRACKERWORKERINTERFACE_H
#define TRACKERWORKERINTERFACE_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

class TrackerWorkerInterface
{

public:
    /*Slot that receives the actual frame*/
    void onFrameGrabbed(cv::Mat);

    /*Emits the original image overlayed with tracking location etc. (Used as a signal)*/
    void trackingPreview(cv::Mat);

    /*Emits a user defined signal (can be used to output something on GPIO pins of camera*/
    void userOutput(int value);

    /*Emits the result of tracking (Used as a signal)*/
    void trackingResult(std::vector<double> data);

};

#endif // TRACKERWORKERINTERFACE_H
