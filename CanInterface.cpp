#include "CanInterface.h"

CanInterface::CanInterface(QObject *parent) : QObject(parent)
{
    CANID_STEPPERMOTORS.append(0x01);
    CANID_STEPPERMOTORS.append(0x02);

    for(auto id : CANID_STEPPERMOTORS){
        message.append(QVector<CanMessage>());
        responseMessage.append(QVector<CanMessage>());
    }

    connect(this, SIGNAL(responseMessageReceived(int)), SLOT(onResponseMessageReceived(int)));
}

CanInterface::~CanInterface()
{
}

void CanInterface::onCanSignal(QString time, quint32 id, QByteArray data)
{
    if (id ==CANID_FROMARDUINO) {//Message from arduino

        //CONVERT TIMESTAMP (to unsigned long from 4 bytes)
        unsigned long int timeStamp;
        timeStamp = ( (data[0] << 24)
                      + (data[1] << 16)
                      + (data[2] << 8)
                      + (data[3] ) );

        QString cameraString =  printBits(data.right(4));

        std::vector<double> data;
        data.push_back(timeStamp);
        for(QChar chr:cameraString){
            data.push_back(QString(chr).toDouble());
        }
        emit(trackingResult(data));
        //emit(cameraTimeStamp(timeStamp, cameraString));
        qDebug() << id << " " << timeStamp << " " << cameraString;
    }else { //Message from steppermotors
        for(int stepperMotorIndex=0;stepperMotorIndex<responseMessage.size();stepperMotorIndex++){
            if(!responseMessage.at(stepperMotorIndex).isEmpty()){
                if((id == responseMessage.at(stepperMotorIndex).first().id) &
                        ((data == responseMessage.at(stepperMotorIndex).first().data) |
                        responseMessage.at(stepperMotorIndex).first().data.isEmpty())){ //check if message is something we are waiting for
                    emit(responseMessageReceived(stepperMotorIndex));
                    break;
                }
            }
        }
    }
}

void CanInterface::onCameraMatrixChanged(QVector<bool> cameraMatrix)
{
    QByteArray writings;
    writings.resize(cameraMatrix.count()/8);
    writings.fill(0);

    for(int b=0; b<cameraMatrix.count(); ++b)
        writings[b/8] = ( writings.at(b/8) | ((cameraMatrix[b]?1:0)<<(b%8)));

    qDebug()<<printBits(writings) << writings.size();

    emit(sendMessage(CANID_ARDUINO_CAMERAMATRIXCHANGED, writings));
}

void CanInterface::onFrameRateChanged(double frameRate)
{
    QByteArray writings;
    writings.resize(2);
    writings.fill(0);

    writings[0] = int(frameRate/10);
    writings[1] = int(frameRate) % 10;

    qDebug()<< "CanInterface: Setting FrameRate to " << frameRate;

    emit(sendMessage(CANID_ARDUINO_FRAMERATE, writings));
}

void CanInterface::onStopCameraMatrix()
{
    QByteArray writings;
    writings.fill(0);
    qDebug()<< "Stopping camera matrix";
    emit(sendMessage(CANID_ARDUINO_STOPRECORDING, writings));
}

void CanInterface::onResumeCameraMatrix()
{
    QByteArray writings;
    writings.fill(0);
    qDebug()<< "Resume camera matrix";
    emit(sendMessage(CANID_ARDUINO_RESUMERECORDING, writings));
}

static QByteArray dataFromHex(const QString &hex)
{
    QByteArray line = hex.toLatin1();
    line.replace(' ', QByteArray());
    return QByteArray::fromHex(line);
}


void CanInterface::onInitializeStepperMotors()
{
    initializeStepperMotor(0);
    initializeStepperMotor(1);
}

void CanInterface::onMoveStepperMotor(int motorIndex, int position)
{
    QByteArray positionMessage;
    positionMessage.append(QByteArrayLiteral("\x23\x7A\x60\x00"));
    //int ePosition = qToBigEndian(position) >> 16;
    positionMessage.append((char*) &position, 4); //appends the first 2 bytes, which correspond to the position
    positionMessage.append((char)0);
    positionMessage.append((char)0);

    qDebug()<< "Motor: " << motorIndex << " Target position: " << position <<  " PositionMessage:" << positionMessage.toHex();
    message[motorIndex].clear();
    responseMessage[motorIndex].clear();

//    message[motorIndex].append(CanMessage{.id = 0x600+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x2B\x40\x60\x00\x0F\x01\x00\x00")});  //halt on
//    responseMessage[motorIndex].append(CanMessage{.id = 0x580+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x60\x40\x60\x00\x00\x00\x00\x00")});  //response
//    message[motorIndex].append(CanMessage{.id = 0x600+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x2B\x40\x60\x00\x0F\x00\x00\x00")});  //halt off (?)
//    responseMessage[motorIndex].append(CanMessage{.id = 0x580+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x60\x40\x60\x00\x00\x00\x00\x00")});  //response 2

    message[motorIndex].append(CanMessage{.id = 0x600+CANID_STEPPERMOTORS.at(motorIndex), .data = positionMessage});
    responseMessage[motorIndex].append(CanMessage{.id = 0x580+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x60\x7A\x60\x00\x00\x00\x00\x00")});
    message[motorIndex].append(CanMessage{.id = 0x600+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x2B\x40\x60\x00\x3F\x00\x00\x00")});
    responseMessage[motorIndex].append(CanMessage{.id = 0x580+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x60\x40\x60\x00\x00\x00\x00\x00")});
    message[motorIndex].append(CanMessage{.id = 0x600+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x2B\x40\x60\x00\x0F\x00\x00\x00")});
    responseMessage[motorIndex].append(CanMessage{.id = 0x580+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x60\x40\x60\x00\x00\x00\x00\x00")});
    emit(sendMessage(message.at(motorIndex).first().id, message.at(motorIndex).first().data));

}

void CanInterface::onChangeMotorSpeed(int motorIndex, int speed)
{
    QByteArray speedMessage;
    speedMessage.append(QByteArrayLiteral("\x23\x81\x60\x00"));
    speedMessage.append((char*) &speed, 4); //appends the first 2 bytes, which correspond to the position
    speedMessage.append((char)0);
    speedMessage.append((char)0);

    qDebug()<< "target speed " << speed <<  " SpeedMessage:" << speedMessage.toHex();
    message[motorIndex].clear();
    responseMessage[motorIndex].clear();
    message[motorIndex].append(CanMessage{.id = 0x600+CANID_STEPPERMOTORS.at(motorIndex), .data = speedMessage});
    responseMessage[motorIndex].append(CanMessage{.id = 0x580+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x60\x81\x60\x00\x00\x00\x00\x00")});
    emit(sendMessage(message.at(motorIndex).first().id, message.at(motorIndex).first().data));
}

void CanInterface::onResponseMessageReceived(int motorIndex)
{
//    qDebug() << responseMessage[motorIndex].at(0).id << " " << responseMessage[motorIndex].at(0).data.toHex();
    responseMessage[motorIndex].pop_front();
    message[motorIndex].pop_front();
    if(!message.at(motorIndex).isEmpty()){ //last entry

//        while(responseMessage.at(motorIndex).first().data.isEmpty()){ //dispatch all messages that don't require a response (empty bytearray)
//            emit(sendMessage(message.at(motorIndex).first().id, message.at(motorIndex).first().data));
//            responseMessage[motorIndex].pop_front();
//            message[motorIndex].pop_front();
//        }


        emit(sendMessage(message.at(motorIndex).first().id, message.at(motorIndex).first().data));
    }
}

QString CanInterface::printBits(QByteArray data)
{
    QString bitString;
    for(int byteIndex=3;byteIndex>=0;byteIndex--){
        for(int bitIndex=0;bitIndex<8;bitIndex++){
            bitString.prepend(QString::number(((data.at(byteIndex)) & (1<<(bitIndex)))!= 0));
        }
    }
    return bitString;
}

void CanInterface::initializeStepperMotor(int motorIndex)
{
    message[motorIndex].clear();
    responseMessage[motorIndex].clear();

    message[motorIndex].append(CanMessage{.id = 0x600+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x2B\x40\x60\x00\x06\x00")}); //Ready to Switch On
    responseMessage[motorIndex].append(CanMessage{.id = 0x580+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x60\x40\x60\x00\x00\x00\x00\x00")}); //Response: OK

    message[motorIndex].append(CanMessage{.id = 0x600+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x2B\x40\x60\x00\x07\x00")}); //Switch On

    //This is somehow weird. What do these responses mean?:
    //responseMessage[motorIndex].append(CanMessage{.id = 0x181, .data = QByteArrayLiteral("\x33\x06\x00\x00\x00\x00\x00\x00")}); //Response: OK
    //responseMessage[motorIndex].append(CanMessage{.id = 0x181, .data = QByteArrayLiteral("\x33\x26\x00\x00\x00\x00\x00\x00")}); //Response: OK
    responseMessage[motorIndex].append(CanMessage{.id = 0x580+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x60\x40\x60\x00\x00\x00\x00\x00")}); //Response: OK
//    responseMessage[motorIndex].append(CanMessage{.id = 0x181, .data = QByteArray()}); //Response: OK //any data!!

    message[motorIndex].append(CanMessage{.id = 0x600+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x2B\x40\x60\x00\x0F\x00")}); //Operation enabled
    responseMessage[motorIndex].append(CanMessage{.id = 0x580+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x60\x40\x60\x00\x00\x00\x00\x00")}); //Response: OK

    message[motorIndex].append(CanMessage{.id = 0x600+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x2F\x60\x60\x00\x01\x00\x00\x00")}); //Position Mode
    responseMessage[motorIndex].append(CanMessage{.id = 0x580+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x60\x60\x60\x00\x00\x00\x00\x00")});
    emit(sendMessage(message.at(motorIndex).first().id, message.at(motorIndex).first().data));

}

void CanInterface::disableStepperMotor(int motorIndex)
{
    message[motorIndex].clear();
    responseMessage[motorIndex].clear();

    message[motorIndex].append(CanMessage{.id = 0x600+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x2B\x40\x60\x00\x06\x00")}); //Ready to Switch On
    responseMessage[motorIndex].append(CanMessage{.id = 0x580+CANID_STEPPERMOTORS.at(motorIndex), .data = QByteArrayLiteral("\x60\x40\x60\x00\x00\x00\x00\x00")}); //Response: OK
    emit(sendMessage(message.at(motorIndex).first().id, message.at(motorIndex).first().data));
}


