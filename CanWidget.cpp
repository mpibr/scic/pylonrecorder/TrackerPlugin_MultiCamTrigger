#include "CanWidget.h"
#include "ui_CanWidget.h"
#include "connectdialog.h"

#include <QCanBus>
#include <QCanBusFrame>
#include <QCloseEvent>
#include <QDesktopServices>
#include <QtDebug>
#include <QTimer>

CanWidget::CanWidget(QWidget *parent) : QWidget(parent)
{
    m_ui = new Ui::CanWidget();
    m_ui->setupUi(this);

    m_connectDialog = new ConnectDialog;

    m_status = new QLabel;
    m_ui->statusBar->addWidget(m_status);

    m_ui->sendMessagesBox->setEnabled(false);

    initActionsConnections();
    //QTimer::singleShot(50, m_connectDialog, &ConnectDialog::show);
    QTimer::singleShot(50, m_connectDialog, &ConnectDialog::ok);

    //connect(m_ui->sendButton, &QPushButton::clicked, this, &CanWidget::sendMessage);
    connect(m_ui->sendButton, SIGNAL(clicked(bool)), this, SLOT(sendMessage()));
}

CanWidget::~CanWidget()
{
    qDebug()<<"Deleting CanWidget";

    QTime dieTime= QTime::currentTime().addSecs(1); //Give some Time to dispatch Can Messages on destruction
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    delete m_canDevice;
    delete m_connectDialog;
    delete m_ui;
}

void CanWidget::showStatusMessage(const QString &message)
{
    m_status->setText(message);
}

void CanWidget::initActionsConnections()
{
    m_ui->actionConnect->setEnabled(true);
    m_ui->actionDisconnect->setEnabled(false);
    m_ui->actionQuit->setEnabled(true);

    connect(m_ui->actionConnect, &QAction::triggered, m_connectDialog, &ConnectDialog::show);
    connect(m_connectDialog, &QDialog::accepted, this, &CanWidget::connectDevice);
    connect(m_ui->actionDisconnect, &QAction::triggered, this, &CanWidget::disconnectDevice);
    connect(m_ui->actionQuit, &QAction::triggered, this, &QWidget::close);
    connect(m_ui->actionAboutQt, &QAction::triggered, qApp, &QApplication::aboutQt);
    connect(m_ui->actionClearLog, &QAction::triggered, m_ui->receivedMessagesEdit, &QTextEdit::clear);
    connect(m_ui->actionPluginDocumentation, &QAction::triggered, this, []() {
        QDesktopServices::openUrl(QUrl("http://doc.qt.io/qt-5/qtcanbus-backends.html#can-bus-plugins"));
    });
}

void CanWidget::receiveError(QCanBusDevice::CanBusError error) const
{
    switch (error) {
    case QCanBusDevice::ReadError:
    case QCanBusDevice::WriteError:
    case QCanBusDevice::ConnectionError:
    case QCanBusDevice::ConfigurationError:
    case QCanBusDevice::UnknownError:
        qWarning() << m_canDevice->errorString();
    default:
        break;
    }
}

void CanWidget::connectDevice()
{
    const ConnectDialog::Settings p = m_connectDialog->settings();

    QString errorString;
    m_canDevice = QCanBus::instance()->createDevice(p.backendName, p.deviceInterfaceName,
                                                    &errorString);
    if (!m_canDevice) {
        showStatusMessage(tr("Error creating device '%1', reason: '%2'")
                          .arg(p.backendName).arg(errorString));
        return;
    }

    connect(m_canDevice, &QCanBusDevice::errorOccurred,
            this, &CanWidget::receiveError);
    connect(m_canDevice, &QCanBusDevice::framesReceived,
            this, &CanWidget::checkMessages);
    connect(m_canDevice, &QCanBusDevice::framesWritten,
            this, &CanWidget::framesWritten);

    if (p.useConfigurationEnabled) {
        for (const ConnectDialog::ConfigurationItem &item : p.configurations)
            m_canDevice->setConfigurationParameter(item.first, item.second);
    }

    if (!m_canDevice->connectDevice()) {
        showStatusMessage(tr("Connection error: %1").arg(m_canDevice->errorString()));

        delete m_canDevice;
        m_canDevice = nullptr;
    } else {
        m_ui->actionConnect->setEnabled(false);
        m_ui->actionDisconnect->setEnabled(true);

        m_ui->sendMessagesBox->setEnabled(true);

        QVariant bitRate = m_canDevice->configurationParameter(QCanBusDevice::BitRateKey);
        if (bitRate.isValid()) {
            showStatusMessage(tr("Backend: %1, connected to %2 at %3 kBit/s")
                    .arg(p.backendName).arg(p.deviceInterfaceName)
                    .arg(bitRate.toInt() / 1000));
        } else {
            showStatusMessage(tr("Backend: %1, connected to %2")
                    .arg(p.backendName).arg(p.deviceInterfaceName));
        }
    }
}

void CanWidget::disconnectDevice()
{
    if (!m_canDevice)
        return;

    m_canDevice->disconnectDevice();
    delete m_canDevice;
    m_canDevice = nullptr;

    m_ui->actionConnect->setEnabled(true);
    m_ui->actionDisconnect->setEnabled(false);

    m_ui->sendMessagesBox->setEnabled(false);

    showStatusMessage(tr("Disconnected"));
}

void CanWidget::framesWritten(qint64 count)
{
//    qDebug() << "Number of frames written:" << count;
}

void CanWidget::closeEvent(QCloseEvent *event)
{
    m_connectDialog->close();
    event->accept();
}

static QString frameFlags(const QCanBusFrame &frame)
{
    if (frame.hasBitrateSwitch() && frame.hasErrorStateIndicator())
        return QStringLiteral(" B E ");
    if (frame.hasBitrateSwitch())
        return QStringLiteral(" B - ");
    if (frame.hasErrorStateIndicator())
        return QStringLiteral(" - E ");
    return QStringLiteral(" - - ");
}

void CanWidget::checkMessages()
{
    if (!m_canDevice)
        return;

    while (m_canDevice->framesAvailable()) {
        const QCanBusFrame frame = m_canDevice->readFrame();

        QString view;
        if (frame.frameType() == QCanBusFrame::ErrorFrame)
            view = m_canDevice->interpretErrorFrame(frame);
        else
            view = frame.toString();

        const QString time = QString::fromLatin1("%1.%2  ")
                .arg(frame.timeStamp().seconds(), 10, 10, QLatin1Char(' '))
                .arg(frame.timeStamp().microSeconds() / 100, 4, 10, QLatin1Char('0'));

        const QString flags = frameFlags(frame);

        emit(canSignal(time, frame.frameId(), frame.payload()));
        m_ui->receivedMessagesEdit->append(time + flags + view);
    }
}

static QByteArray dataFromHex(const QString &hex)
{
    QByteArray line = hex.toLatin1();
    line.replace(' ', QByteArray());
    return QByteArray::fromHex(line);
}

void CanWidget::sendMessage() const
{
    if (!m_canDevice)
        return;

    QByteArray writings = dataFromHex(m_ui->lineEdit->displayText());

    QCanBusFrame frame;
    const int maxPayload = m_ui->fdBox->checkState() ? 64 : 8;
    writings.truncate(maxPayload);
    frame.setPayload(writings);

    qint32 id = m_ui->idEdit->displayText().toInt(nullptr, 16);
    if (!m_ui->effBox->checkState() && id > 2047) //11 bits
        id = 2047;

    frame.setFrameId(id);
    frame.setExtendedFrameFormat(m_ui->effBox->checkState());
    frame.setFlexibleDataRateFormat(m_ui->fdBox->checkState());
    frame.setBitrateSwitch(m_ui->bitrateSwitchBox->checkState());

    if (m_ui->remoteFrame->isChecked())
        frame.setFrameType(QCanBusFrame::RemoteRequestFrame);
    else if (m_ui->errorFrame->isChecked())
        frame.setFrameType(QCanBusFrame::ErrorFrame);
    else
        frame.setFrameType(QCanBusFrame::DataFrame);

    m_canDevice->writeFrame(frame);
}

void CanWidget::sendMessage(qint32 id, QByteArray writings) const
{
    if (!m_canDevice)
        return;

    QCanBusFrame frame;
    const int maxPayload = m_ui->fdBox->checkState() ? 64 : 8;
    writings.truncate(maxPayload);
    frame.setPayload(writings);

    frame.setFrameId(id);
    frame.setExtendedFrameFormat(m_ui->effBox->checkState());
    frame.setFlexibleDataRateFormat(m_ui->fdBox->checkState());
    frame.setBitrateSwitch(m_ui->bitrateSwitchBox->checkState());

    if (m_ui->remoteFrame->isChecked())
        frame.setFrameType(QCanBusFrame::RemoteRequestFrame);
    else if (m_ui->errorFrame->isChecked())
        frame.setFrameType(QCanBusFrame::ErrorFrame);
    else
        frame.setFrameType(QCanBusFrame::DataFrame);

//    qDebug()<<"CanWidget: sending "<< frame.toString();
    m_canDevice->writeFrame(frame);
}
