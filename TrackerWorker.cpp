#include "TrackerWorker.h"

TrackerWorker::TrackerWorker(QObject* parent)
    : QObject(parent)
{
    qRegisterMetaType < QVector<double> >("QVectorDouble");

    rois.push_back(QRect(0,0,100,100));

    thresholds.push_back(255);
    areaConstraints.push_back(100);
    areaConstraints.push_back(50000);

    //fgbg_mog = bgsegm::createBackgroundSubtractorMOG();
    fgbg = createBackgroundSubtractorMOG2(backgroundHistory, varThreshold, detectShadows);

    cameraPositionPreview.x = 0;
    cameraPositionPreview.y = 0;
    cameraPositionPreview.width = 100;
    cameraPositionPreview.height = 100;
}

void TrackerWorker::onFrameGrabbed(cv::Mat src)
{
    using namespace cv;

    if (doCalculateBackground) {
        backgroundLearningRate = -1;
        nBackgroundImages++;

        if(nBackgroundImages == backgroundHistory){
            doCalculateBackground = false;
            qDebug()<<"TrackerWorker: Collected " << nBackgroundImages << " frames for background subtraction";
            nBackgroundImages = 0;
            backgroundLearningRate = 0;
        }
    }

    int blobMinArea = areaConstraints.at(0);
    int blobMaxArea = areaConstraints.at(1);

    Point centerLoc(0,0);

    // Apply background subtraction (and train in case is learning)
    fgbg->apply(src.clone(), foreground, backgroundLearningRate);

//    Mat im_gray ;
//    cvtColor(src, im_gray, CV_RGB2GRAY);

//    /*Blob*/
//    Mat blobMask;
//    Mat blobRegion = im_gray.rowRange(rois.at(0).y(),rois.at(0).y() + rois.at(0).height()).colRange(rois.at(0).x(),rois.at(0).x() + rois.at(0).width());
//    if(isInverted){
//        cv::threshold(blobRegion, blobMask, thresholds.at(0), 1, THRESH_BINARY);
//    }else{
//        cv::threshold(blobRegion, blobMask, thresholds.at(0), 1, THRESH_BINARY_INV);
//    }

    std::vector<Vec4i> blobHierarchy;
    std::vector<std::vector<Point> > blobContours;
    std::vector<Point> blobContour;
    findContours(foreground, blobContours, blobHierarchy, cv::RetrievalModes::RETR_TREE,  cv::ContourApproximationModes::CHAIN_APPROX_SIMPLE, Point(0, 0) );
    for(uint i=0;i<blobContours.size();i++){
        if((contourArea(blobContours[i])>=blobMinArea) & (contourArea(blobContours[i])<=blobMaxArea)){
            blobContour = blobContours[i];
            for(uint j=0;j<blobContour.size();j++){
                blobContour.at(j) = blobContour.at(j) + Point2i(rois.at(0).x(), rois.at(0).y()) ;
            }
            break;
        }
    }

    RotatedRect blobRect;
    if(blobContour.size()>4){
        /*Determine centroid by fitting an ellipse*/
        blobRect = fitEllipse(blobContour);
        centerLoc = blobRect.center;
    }

    /*Generate preview image*/
    Mat previewIm(src.size(), CV_8U, double(0));
    previewIm(Rect(rois.at(0).x(),rois.at(0).y(), rois.at(0).width(), rois.at(0).height())) = foreground*255;

    Mat outputIm;
    if(showBinary){
        if(foreground.size().width > 0 & foreground.size().height > 0 ){
            outputIm = foreground.clone();
        }
    }else{
        outputIm = src.clone();
    }

    circle(outputIm, centerLoc, 10, Scalar(0,255,0), -1, 8, 0);

    //std::vector<std::vector<Point> > pContourV = std::vector<std::vector<Point> >(1, pContour);
    //drawContours(outputIm, pContourV, -1, Scalar(0,255,0), 2, 8);

    ellipse(outputIm, blobRect, Scalar(0,255,0), 10, 8);

    rectangle(outputIm, cameraPositionPreview, Scalar(255,0,255));

    std::vector<double> result;
    result.push_back(centerLoc.x);
    result.push_back(centerLoc.y);

   // qDebug() << result;

    //Check if hot Area contains position

//    if(centerLoc.x!=0 | centerLoc.y!=0){

//        for(int i=0;i<hotAreas.size();i++){
//            bool currentlyContainsLocation = hotAreas.at(i).contains(centerLoc.x, centerLoc.y);
//            /*Detect transitions into and out of hot area*/
//            if(!isInHotArea.at(i) & currentlyContainsLocation){
//                qDebug()<< "Blob entered hot area";
//                isInHotArea[i] = true;
//                emit(userOutput(1));
//                //Activate specific cameras
//            }else if(isInHotArea.at(i) & !currentlyContainsLocation){
//                qDebug()<< "Blob left hot area";
//                isInHotArea[i] = false;
//                emit(userOutput(0));
//                //Deactivate specific cameras
//            }
//        }
//    }

    emit(trackingResult(result));
    emit(trackingPreview(outputIm));
}

void TrackerWorker::onThresholdChanged(int index, int value)
{
    thresholds.replace(index, value);
    varThreshold = value; //for background subtraction
    fgbg.release();
    fgbg = createBackgroundSubtractorMOG2(backgroundHistory, varThreshold, detectShadows);
}

void TrackerWorker::onAreaConstraintsChanged(int index, int area)
{
    areaConstraints.replace(index, area);
}

void TrackerWorker::onRoiChanged(int index, QRect roi)
{
    rois.replace(index, roi);
}

void TrackerWorker::onHotAreaChanged(int index, QRect hotArea)
{
    hotAreas.replace(index, hotArea);
}

void TrackerWorker::onUserInput(bool isHigh)
{
    //Nothing happens here yet
}

void TrackerWorker::onCameraPositionChanged(QRect cameraPosition)
{
    cameraPositionPreview.x = cameraPosition.x();
    cameraPositionPreview.y = cameraPosition.y();
    cameraPositionPreview.width = cameraPosition.width();
    cameraPositionPreview.height = cameraPosition.height();
}

void TrackerWorker::onShowBinary(bool showBinary)
{
    this->showBinary = showBinary;
}

void TrackerWorker::onInvertedBlob(bool isInverted)
{
    this->isInverted = isInverted;
}

void TrackerWorker::onChangeNumberOfHotAreas(QVector<QRect> hotAreas)
{
    this->hotAreas = hotAreas;
    isInHotArea.resize(hotAreas.size());
    for(int i=0;i<isInHotArea.size();i++)
        isInHotArea[i]=false;
}

void TrackerWorker::onCollectBackground()
{
    doCalculateBackground = true;
    qDebug() << "TrackerWorker: collecting background";
}
