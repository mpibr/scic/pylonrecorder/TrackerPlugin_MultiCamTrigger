#include "ResizeTraits.h"

/// Determines whether an item is manually resizeable.
bool ResizeTraits::isGraphicsItemResizeable(QGraphicsItem * item) {
    auto resizeableItem1 = dynamic_cast<QGraphicsEllipseItem*>(item);
    if(resizeableItem1){
        return resizeableItem1;
    }
    auto resizeableItem2 = dynamic_cast<QGraphicsRectItem*>(item);
    if(resizeableItem2){
        return resizeableItem2;
    }
    return false;
}

/// Gives the rectangle one can base the resize operations on for an item
QRectF ResizeTraits::rectFor(QGraphicsItem * item) {
    auto resizeableItem1 = dynamic_cast<QGraphicsEllipseItem*>(item);
    if(resizeableItem1){
        return resizeableItem1->rect();
    }
    auto resizeableItem2 = dynamic_cast<QGraphicsRectItem*>(item);
    if(resizeableItem2){
        return resizeableItem2->rect();
    }
    return QRect();
}

/// Sets a new rectangle on the item
void ResizeTraits::setRectOn(QGraphicsItem * item, const QRectF & rect) {
    auto resizeableItem1 = dynamic_cast<QGraphicsEllipseItem*>(item);
    if (resizeableItem1){
        return resizeableItem1->setRect(rect);
    }
    auto resizeableItem2 = dynamic_cast<QGraphicsRectItem*>(item);
    if (resizeableItem2){
        return resizeableItem2->setRect(rect);
    }
}

QPen ResizeTraits::penFor(QGraphicsItem *item)
{
    auto resizeableItem = dynamic_cast<QGraphicsRectItem*>(item);
    if (resizeableItem){
        /*Use the same Pen as the parent for the bounding rectangle*/
        QPen pen = resizeableItem->pen();
        QColor color = pen.color();
        color.setAlpha(100);
        pen.setColor(color);
        pen.setWidth(pen.width()*10);
        pen.setJoinStyle(Qt::MiterJoin);
        return pen;
    }else{
        return QPen(QColor(255, 0, 0, 128), Qt::SolidLine);
    }
}
