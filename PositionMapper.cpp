#include "PositionMapper.h"

PositionMapper::PositionMapper(QObject *parent) : QObject(parent)
{
    connect(this, &PositionMapper::trackingResult, [this]( std::vector<double> result) {
        this->onTrackingResult(QVector<double>::fromStdVector(result), TrackingSource::camera);
    });

    connect(this, &PositionMapper::socketTrackingResult, [this]( std::vector<double> result) {
        this->onTrackingResult(QVector<double>::fromStdVector(result), TrackingSource::socket);
    });

}

void PositionMapper::onEnableAutoTracking(bool isEnabled)
{
    isAutoTracking = isEnabled;
}

void PositionMapper::onTrackingResult(QVector<double> result, TrackingSource source)
{

    bool moveMotor = false;
    switch (source){
    case TrackingSource::camera:{
        //qDebug()<< "Result from camera";
        if(pastResults.size() == ringSize){
            pastResults.pop_front();
        }
        pastResults.append(result.at(0));

        if(!pastResults.contains(double NAN)){
            moveMotor = true;
        }
        break;
    }
    case TrackingSource::socket:{
        //qDebug()<< "Result from socket";
        if( (pastResults.contains(double NAN)) && (std::isnan(result.at(0))) ){
            moveMotor = true;
            return;
        }
        break;
    }
    }

    if(isAutoTracking && moveMotor){
        double blobX = result.at(0);
        double blobY = result.at(1);

        //TODO: this should be used to plot a preview of the camera array!
        emit(cameraArrayPositionChanged(QRect(0,0,100,100)));


        if((blobX-lastBlobX > 100) | (blobX-lastBlobX < -100) | (blobY-lastBlobY > 100) | (blobY-lastBlobY < -100) ) {

            //        qDebug()<< "Current position: " << blobX << ", " << blobY;

            cv::Mat pMat = (cv::Mat_<double>(3,3) << 1.0911e+03,-1.1498e+04,2.1028e-04,2.4983e+04, 5.3019e+03,0.0025,-6.4470e+06,1.4014e+07,1);
            cv::Mat pos = (cv::Mat_<double>(3,1) << blobX,  blobY, 1);

            cv::Mat x1, x2, xAll;
            cv::multiply(pMat.row(0).col(0), pos.row(0).col(0),x1);
            cv::multiply(pMat.row(1).col(0), pos.row(1).col(0),x2);
            xAll=x1+x2+pMat.row(2).col(0);

            cv::Mat y1, y2, yAll;
            cv::multiply(pMat.row(0).col(1), pos.row(0).col(0),y1);
            cv::multiply(pMat.row(1).col(1), pos.row(1).col(0),y2);
            yAll=y1+y2+pMat.row(2).col(1);

            cv::Mat z1, z2, zAll;
            cv::multiply(pMat.row(0).col(2), pos.row(0).col(0),z1);
            cv::multiply(pMat.row(1).col(2), pos.row(1).col(0),z2);
            zAll=z1+z2+pMat.row(2).col(2);

            cv::Mat tpos1, tpos2;

            cv::divide(xAll,zAll,tpos1);
            cv::divide(yAll,zAll,tpos2);

            qDebug()<< "tracked POSITION: " << pos.at<double>(0,0) << "," << pos.at<double>(1,0);
            qDebug()<< "TARGET POSITION: " << tpos1.at<double>(0,0) << "," << tpos2.at<double>(0,0);
            int xPos = (int)tpos1.at<double>(0,0);
            int yPos = (int)tpos2.at<double>(0,0);

            if(( xPos>0 ) & ( xPos < motor0Max )){
                emit(moveStepperMotor(0, xPos));
            }
            if(( yPos>0 ) & ( yPos < motor1Max )){
                emit(moveStepperMotor(1, yPos));
            }

            lastBlobX = blobX;
            lastBlobY = blobY;
        }

    }

}


void PositionMapper::onMouseClickEvent(QPointF mousePos)
{
    double blobX = mousePos.x();
    double blobY = mousePos.y();

    cv::Mat pMat = (cv::Mat_<double>(3,3) << 1.0911e+03,-1.1498e+04,2.1028e-04,2.4983e+04, 5.3019e+03,0.0025,-6.4470e+06,1.4014e+07,1);
    cv::Mat pos = (cv::Mat_<double>(3,1) << blobX,  blobY, 1);

    cv::Mat x1, x2, xAll;
    cv::multiply(pMat.row(0).col(0), pos.row(0).col(0),x1);
    cv::multiply(pMat.row(1).col(0), pos.row(1).col(0),x2);
    xAll=x1+x2+pMat.row(2).col(0);

    cv::Mat y1, y2, yAll;
    cv::multiply(pMat.row(0).col(1), pos.row(0).col(0),y1);
    cv::multiply(pMat.row(1).col(1), pos.row(1).col(0),y2);
    yAll=y1+y2+pMat.row(2).col(1);

    cv::Mat z1, z2, zAll;
    cv::multiply(pMat.row(0).col(2), pos.row(0).col(0),z1);
    cv::multiply(pMat.row(1).col(2), pos.row(1).col(0),z2);
    zAll=z1+z2+pMat.row(2).col(2);

    cv::Mat tpos1, tpos2;

    cv::divide(xAll,zAll,tpos1);
    cv::divide(yAll,zAll,tpos2);

    qDebug()<< "tracked POSITION: " << pos.at<double>(0,0) << "," << pos.at<double>(1,0);
    qDebug()<< "TARGET POSITION: " << tpos1.at<double>(0,0) << "," << tpos2.at<double>(0,0);
    int xPos = (int)tpos1.at<double>(0,0);
    int yPos = (int)tpos2.at<double>(0,0);

    if(( xPos>0 ) & ( xPos < motor0Max )){
        emit(moveStepperMotor(0, xPos));
    }
    if(( yPos>0 ) & ( yPos < motor1Max )){
        emit(moveStepperMotor(1, yPos));
    }
}

