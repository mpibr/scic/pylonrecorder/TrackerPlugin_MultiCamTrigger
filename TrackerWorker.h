#ifndef TRACKERWORKER_H
#define TRACKERWORKER_H
#include <QDebug>
#include <QtGlobal>
#include <QVector>
#include <QRect>
#include <QTimer>
#include <QSignalMapper>
#include "TrackerWorkerInterface.h"

#include <opencv2/core/core.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/features2d.hpp>
#include "opencv2/bgsegm.hpp" //MOG (1)
#include <QGraphicsSceneMouseEvent>


using namespace cv;

class TrackerWorker : public QObject, public TrackerWorkerInterface
{
    Q_OBJECT

public:
    TrackerWorker(QObject* parent = 0);

private:

    QVector<QRect> rois;
    QVector<QRect> hotAreas;
    QVector<int> thresholds;
    QVector<int> areaConstraints;
    Rect cameraPositionPreview;
    bool showBinary = false;
    bool isInverted = false;
    QVector<bool> isInHotArea;
    QVector<QVector<int>>cameraHotAreas;
    int colorConversion = cv::COLOR_BayerRG2RGB;
    Ptr<BackgroundSubtractor> fgbg;
//    bgsegm::BackgroundSubtractorMOG* fgbg_mog;
    Mat foreground;
    int backgroundHistory = 10;
    int varThreshold = 16;
    bool detectShadows = false;
    bool doCalculateBackground = true;
    bool isBackgroundCalculated = false;
    int nBackgroundImages = 0;
    int backgroundLearningRate = 0;


public slots:
    void onFrameGrabbed(cv::Mat);
    void onThresholdChanged(int index, int value);
    void onAreaConstraintsChanged(int minSize, int maxSize);
    void onRoiChanged(int index, QRect roi);
    void onHotAreaChanged(int index, QRect hotArea);
    void onUserInput(bool isHigh);
    void onCameraPositionChanged(QRect cameraPosition);
    void onShowBinary(bool showBinary);
    void onInvertedBlob(bool isInverted);
    void onChangeNumberOfHotAreas(QVector<QRect> hotAreas); //contains a vector of associated cameras for each hotarea
    void onCollectBackground();
signals:
    void trackingPreview(cv::Mat);
    void trackingRois(QVector<cv::Mat>);
    void trackingResult(std::vector<double> data); //The results are stored in a regular vector for easier communication with non-Qt C++ programs (e.g. through socket)
    void socketTrackingResult(std::vector<double> data); //Forwarded signal from PylonRecorder
    void trackingMessage(std::string message);
    void userOutput(bool value);
};

#endif // TRACKERWORKER_H
