#-------------------------------------------------
#
# Project created by QtCreator 2016-10-24T11:57:07
#
#-------------------------------------------------

TEMPLATE      = lib
CONFIG       += plugin
QT           += serialbus network widgets
TARGET        = $$qtLibraryTarget(TrackerPlugin_MultiCamTrigger)

DESTDIR       = /local_disk/plugins
#DESTDIR       = /home/kretschmerf/build/PylonRecorder-Desktop_Qt_5_9_6_GCC_64bit2-Debug/plugins
#DESTDIR = /home/sepia/build/PylonRecorder_control/plugins


CONFIG += c++11


SOURCES += \
    TrackerWorker.cpp \
    TrackerPlugin_MultiCamTrigger.cpp \
    ResizeHelper.cpp \
    ResizeTraits.cpp \
    bitratebox.cpp \
    connectdialog.cpp \
    CanWidget.cpp \
    CanInterface.cpp \
    PositionMapper.cpp

HEADERS += \
    TrackerWorker.h \
    TrackerPlugin_MultiCamTrigger.h \
    ResizeHelper.h \
    ResizeTraits.h \
    bitratebox.h \
    connectdialog.h \
    CanWidget.h \
    CanInterface.h \
    PositionMapper.h

INCLUDEPATH += /gpfs/scic/software/PylonRecorder/PylonRecorder2

win32 {
    INCLUDEPATH += "D:\\opencv\\build_vc12\\install\\include"

    CONFIG(debug,debug|release) {
        LIBS += -L"D:\\opencv\\build_vc12\\install\\x64\\vc12\\lib" \
            -lopencv_core310 \
            -lopencv_highgui310 \
            -lopencv_imgproc310 \
            -lopencv_features2d310 \
            -lopencv_videoio310 \
            -lopencv_video310 \
            -lopencv_videostab310 \
    }

    CONFIG(release,debug|release) {
        DEFINES += QT_NO_WARNING_OUTPUT QT_NO_DEBUG_OUTPUT
        LIBS += -L"D:\\opencv\\build_vc12\\install\\x64\\vc12\\lib" \
            -lopencv_core310 \
            -lopencv_highgui310 \
            -lopencv_imgproc310 \
            -lopencv_features2d310 \
            -lopencv_videoio310 \
            -lopencv_video310 \
            -lopencv_videostab310 \
    }

    INCLUDEPATH += "C:/Qwt-6.1.3/include"
    LIBS += -L"C:/Qwt-6.1.3/lib" \
        -lqwt
}

unix{
    CONFIG += link_pkgconfig
    PKGCONFIG += opencv4
#    CONFIG += qwt
#    INCLUDEPATH += /usr/include/qwt
    LIBS += -L/usr/lib/x86_64-linux-gnu
   LIBS += -lopencv_video -lopencv_highgui -lopencv_bgsegm
}

FORMS += \
    TrackerPlugin_MultiCamTrigger.ui \
    connectdialog.ui \
    CanWidget.ui

RESOURCES += \
    can.qrc
