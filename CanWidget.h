#ifndef CANWIDGET_H
#define CANWIDGET_H

#include <QCanBusDevice> // for CanBusError
#include <QDebug>
#include <QWidget>
#include <QTime>

class ConnectDialog;

QT_BEGIN_NAMESPACE

class QCanBusFrame;
class QLabel;

namespace Ui {
class CanWidget;
}

QT_END_NAMESPACE

class CanWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CanWidget(QWidget *parent = nullptr);
    ~CanWidget();

public slots:
    void sendMessage(qint32 id, QByteArray writings) const;

private slots:
    void checkMessages();
    void sendMessage() const;
    void receiveError(QCanBusDevice::CanBusError) const;
    void connectDevice();
    void disconnectDevice();
    void framesWritten(qint64);

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    void showStatusMessage(const QString &message);
    void initActionsConnections();

    Ui::CanWidget *m_ui = nullptr;
    QLabel *m_status = nullptr;
    ConnectDialog *m_connectDialog = nullptr;
    QCanBusDevice *m_canDevice = nullptr;

signals:
    void canSignal(QString time, quint32 id, QByteArray data);


};

#endif // CANWIDGET_H
