#ifndef MULTICAMTRIGGER_H
#define MULTICAMTRIGGER_H

#include "TrackerInterface.h"
#include "TrackerWorker.h"
#include "ui_TrackerPlugin_MultiCamTrigger.h"
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QDebug>
#include <QSignalMapper>
#include <QPoint>
#include <QSize>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <QObject>
#include <QDebug>
#include <QSettings>
#include <QFile>
#include <QDir>
#include "ResizeHelper.h"
#include "CanInterface.h"
#include "PositionMapper.h"

namespace Ui {
class TrackerPlugin_MultiCamTrigger;
}

class TrackerPlugin_MultiCamTrigger : public QWidget, public TrackerInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.PylonRecorder.TrackerInterface")
    Q_INTERFACES(TrackerInterface)

public:
    explicit TrackerPlugin_MultiCamTrigger(QWidget *parent = nullptr);
    ~TrackerPlugin_MultiCamTrigger();
    QObject *worker();
    void initializeUI(QLayout* guiTargetLayout, QGraphicsScene* cameraScene, QSize cameraResolution);
    TrackerWorker* trackerWorker;
    QSize cameraResolution = QSize(0,0);

private:
    Ui::TrackerPlugin_MultiCamTrigger *ui;
    QGraphicsScene* cameraScene;
    QGraphicsPixmapItem* pixmapItem;
    QVector<QGraphicsRectItem*> rectItems;
    QVector<QGraphicsRectItem*> hotAreaRectItems;
    QVector<QPen*> plotPens;
    QVector<QPen*> rectPens;
    QVector<QPen*> hotAreaPens;

    /*ui pointers for use with signalmapper*/
    QVector<QSlider*> uiSliders;
    QVector<QLabel*> uiLabels;
    QVector<QCheckBox*> uiCheckboxes;
    QVector<QLineEdit*> uiLineEdits;
    QVector<QRect> hotAreas;
    QRect roi;

    int maxRectWidth = 200;
    const int bitDepth = 8;

    int wndLength = 100;

    bool manualTriggerToggle = true;

    QSignalMapper* signalMapperPosition;
    QSignalMapper* signalMapperCheckBoxes;
    QSignalMapper* signalMapperSliders;
    QSignalMapper* signalMapperLineEdit;

    ResizeHelper* resizeHelper; //Manages resizing of items in scene

    QString settingsFileName;
    void saveSettings();
    void loadSettings();

    CanInterface canInterface;
    PositionMapper positionMapper;

private slots:
    void onThreshSliderMoved(int);
    void onGraphicsItemChanged(QGraphicsItem* item);
    void onAreaConstraintsEditingFinished(int);

    void on_horizontalSlider_xPosition_sliderReleased();
    void on_horizontalSlider_yPosition_sliderReleased();
    void on_pushButton_triggerCameras_clicked();
    void on_checkBox_showCanDetails_clicked(bool checked);

    void on_horizontalSlider_speed_sliderReleased();
    void on_pushButton_collectBackground_clicked();

    void on_checkBox_autoTracking_clicked(bool checked);

    void on_lineEdit_frameRate_editingFinished();

signals:
    void changeNumberOfHotAreas(QVector<QRect> hotAreas);
    void roiChanged(int index, QRect roi);
    void hotAreaChanged(int index, QRect roi);
    void thresholdChanged(int index, int value);
    void areaContraintsChanged(int index, int value);
    void initializeStepperMotors();
    void moveStepperMotor(int motorIndex, int value);
    void changeMotorSpeed(int motorIndex, int value);
    void cameraMatrixChanged(QVector<bool>cameraMatrix);
    void frameRateChanged(double frameRate);
    void enableAutoTracking(bool);
    void trackingMessage(std::string message); //for logfile
    void collectBackground();

};

#endif // MULTICAMTRIGGER_H
