#ifndef CANINTERFACE_H
#define CANINTERFACE_H

#include <QObject>
#include <QDebug>
#include <QTextCodec>
#include <QBitArray>
#include <QStateMachine>
#include <QDataStream>
#include <QTime>
#include <QCoreApplication>
#include <QtEndian>
#include <QTimer>

const quint32 CANID_FROMARDUINO = 0x7B;
const quint32 CANID_ARDUINO_CAMERAMATRIXCHANGED = 0x620;
const quint32 CANID_ARDUINO_STOPRECORDING = 0x621;
const quint32 CANID_ARDUINO_RESUMERECORDING = 0x622;
const quint32 CANID_ARDUINO_FRAMERATE = 0x623;

//Interprets the actual can messages
struct CanMessage{
    quint32 id;
    QByteArray data;
};

class CanInterface : public QObject
{
    Q_OBJECT
public:
    explicit CanInterface(QObject *parent = nullptr);
    ~CanInterface();
    void disableStepperMotor(int motorIndex);


private:
    QString printBits(QByteArray data);
    QVector<QVector<CanMessage>> responseMessage;
    QVector<QVector<CanMessage>> message;
    QVector<quint32> CANID_STEPPERMOTORS;
    void initializeStepperMotor(int motorIndex);

signals:
    void sendMessage(qint32 id, QByteArray writings) const;
    void responseMessageReceived(int motorIndex);
    void trackingResult(std::vector<double> data); //for logfile
    void trackingMessage(std::string message);


public slots:
    void onCanSignal(QString time, quint32 id, QByteArray data);
    void onCameraMatrixChanged(QVector<bool>cameraMatrix);
    void onFrameRateChanged(double frameRate);
    void onStopCameraMatrix();
    void onResumeCameraMatrix();
    void onInitializeStepperMotors();
    void onMoveStepperMotor(int motorIndex, int position);
    void onChangeMotorSpeed(int motorIndex, int speed);
    void onResponseMessageReceived(int motorIndex);
};

#endif // CANINTERFACE_H
