# TrackerPlugin_MultiCamTrigger
Videotracking of a blob. Emits a singal on the GPIO of a camera when the center of mass is within a certain defined area.


0x601 sender
motor1 = 0x581

# Servo motor control with OpenCAN Communication

## POWER ON

|CAN command                  | meaning                              |
|-----------------------------|--------------------------------------|
| 622 2B 40 60 00 06 00       | Ready to Switch On                   |
| 5A2 60 40 60 00 00 00 00 00 | Response: OK                         |
| 622 40 41 60 00             | Request of the status word           |
| 5A2 4B 41 60 00 21 02 00 00 | Response: Ready to Switch On         |
|                             |                                      |
| 622 2B 40 60 00 07 00       | Switch On (power drive on)           |
| 5A2 60 40 60 00 00 00 00 00 | Response: OK                         |
| 622 40 41 60 00             | Request of the status word           |
| 5A2 4B 41 60 00 33 02 00 00 | Response: Switch On, Voltage Enabled |
|                             |                                      |
| 622 2B 40 60 00 0F 00       | Operation Enabled                    |
| 5A2 60 40 60 00 00 00 00 00 | Response: OK                         |
| 622 40 41 60 00             | Request of the status word           |
| 5A2 4B 41 60 00 37 02 00 00 | Response: Operation & Voltage Enabled|

## OPERATIONAL MODE: Profile Positioning

|CAN command                  | meaning                              |
|-----------------------------|--------------------------------------|
| 622 2F 60 60 00 01          | Modus: Profile Position (PP)         |
| 5A2 60 60 60 00 00 00 00 00 | Response: OK                         |

## MOVING TO POSITION:

|CAN command                  | meaning                              |
|-----------------------------|--------------------------------------|
| 622 23 7A 60 00 45 23 01 00 | Endposition to 0x12345               |
| 5A2 60 7A 60 00 00 00 00 00 | Response: OK                         |
| 622 2b 40 60 00 1F 00       | Start of movement                    |
| 5A2 60 40 60 00 00 00 00 00 | Response: OK                         |
| 622 2b 40 60 00 0F 00       | Reset of the start bit               |
| 5A2 60 40 60 00 00 00 00 00 | Response: OK                         |

## Explanation of the data bytes

Explanation of the data to be entered in hex:

|field        | meaning                                                 |
|-------------|---------------------------------------------------------|
| 622         | The COB ID for sending SDOs to the CANopen node 34(dec) |
| 2B          | Command for writing a SDO with 2 data bytes             |
| 40          | Low-order byte of the SDO ID 0x6040                     |
| 60          | High-order byte of the SDO ID 0x6040                    |
| 00          | Subindex 0x00                                           |
| 00,06,07,0f | Least significant byte of the control word              |

It is important to note that the data in CANopen are always transmitted in Intel
notation, i.e. the low-order bytes first.

[CANopen_Reference_Manual_V2.5](https://en.nanotec.com/fileadmin/files/Handbuecher/NanoCAN/CANopen_Reference_Manual_V2.5.pdf)
