#ifndef ResizeTraits_H
#define ResizeTraits_H
#include <QGraphicsItem>
#include <QRectF>
#include <QPen>
#include <QDebug>

class ResizeTraits
{
public:
    static bool isGraphicsItemResizeable(QGraphicsItem* item);
    static QRectF rectFor(QGraphicsItem* item);
    static void setRectOn(QGraphicsItem* item, const QRectF& rect);
    static QPen penFor(QGraphicsItem* item);
};

#endif // ResizeTraits_H
