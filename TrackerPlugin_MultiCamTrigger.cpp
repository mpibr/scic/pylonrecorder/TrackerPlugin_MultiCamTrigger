#include "TrackerPlugin_MultiCamTrigger.h"

TrackerPlugin_MultiCamTrigger::TrackerPlugin_MultiCamTrigger(QWidget *parent)
    : QWidget(parent)
{

    isLogFileRecording = true; //enable writing to log file in GUI

    settingsFileName = QDir::currentPath() + "/settings.ini";

    loadSettings();
    if(hotAreaRectItems.size()==0){ //if no hot areas were loaded save new settings
        //QTimer::singleShot(1000, this, saveSettings()); //start delayed so camera has initialized
    }

    ui = new Ui::TrackerPlugin_MultiCamTrigger();
    trackerWorker = new TrackerWorker();
    ui->setupUi(this);

    resizeHelper = new ResizeHelper(this);



    ui->label_pupil->setStyleSheet("QLabel { color : green; }");

    pixmapItem = new QGraphicsPixmapItem();

    rectPens.push_back(new QPen(Qt::green));
    for(int i =0;i<rectPens.size();i++){
        rectPens.at(i)->setWidth(2);
    }

    for(int i=0;i<hotAreas.size();i++){
        hotAreaPens.push_back(new QPen(Qt::magenta));
        hotAreaPens.at(i)->setWidth(2);
        /*Prepare hot spot rects*/
        hotAreaRectItems.push_back(new QGraphicsRectItem());
        hotAreaRectItems.at(i)->setPen(*hotAreaPens.at(i));
        hotAreaRectItems.at(i)->setFlag(QGraphicsItem::ItemIsSelectable, true);
        hotAreaRectItems.at(i)->setToolTip(QString::number(i));
    }

    uiSliders.push_back(ui->horizontalSlider_threshold);
    uiLabels.push_back(ui->label_threshold_1);

    uiLineEdits.push_back(ui->lineEdit_PupilMinArea);
    uiLineEdits.push_back(ui->lineEdit_PupilMaxArea);

    /*Position signalmapper*/
    signalMapperPosition = new QSignalMapper(this);

    /*Prepare rects*/
    rectItems.push_back(new QGraphicsRectItem());
    rectItems.at(0)->setPen(*rectPens.at(0));
    rectItems.at(0)->setFlag(QGraphicsItem::ItemIsSelectable, true);
    rectItems.at(0)->setToolTip("ROI" + QString::number(0));

    /*Sliders signalmapper*/
    signalMapperSliders = new QSignalMapper(this);
    /*Connect checkboxes and sliders*/
    connect(ui->horizontalSlider_threshold, SIGNAL(valueChanged(int)), signalMapperSliders, SLOT(map()));
    signalMapperSliders->setMapping(ui->horizontalSlider_threshold, 0);
    connect(signalMapperSliders, SIGNAL(mapped(int)), this, SLOT(onThreshSliderMoved(int)));

    //LineEdit signalmapper
    //The signalmapper is an overkill at this point but since we used them for everything else, we'll stick to them for now
    signalMapperLineEdit = new QSignalMapper(this);
    connect(ui->lineEdit_PupilMinArea, SIGNAL(editingFinished()), signalMapperLineEdit, SLOT(map()));
    connect(ui->lineEdit_PupilMaxArea, SIGNAL(editingFinished()), signalMapperLineEdit, SLOT(map()));
    signalMapperLineEdit->setMapping(ui->lineEdit_PupilMinArea, 0);
    signalMapperLineEdit->setMapping(ui->lineEdit_PupilMaxArea, 1);
    connect(signalMapperLineEdit, SIGNAL(mapped(int)), this, SLOT(onAreaConstraintsEditingFinished(int)));

    //Connection TrackingGui - TrackingWorker
    connect(this, SIGNAL(changeNumberOfHotAreas(QVector<QRect>)), trackerWorker, SLOT(onChangeNumberOfHotAreas(QVector<QRect>)));
    connect(this, SIGNAL(thresholdChanged(int, int)), trackerWorker, SLOT(onThresholdChanged(int, int)));
    connect(this, SIGNAL(hotAreaChanged(int,QRect)), trackerWorker, SLOT(onHotAreaChanged(int,QRect)));
    connect(this, SIGNAL(roiChanged(int,QRect)), trackerWorker, SLOT(onRoiChanged(int,QRect)));
    connect(this, SIGNAL(areaContraintsChanged(int,int)), trackerWorker, SLOT(onAreaConstraintsChanged(int,int)));
    connect(ui->checkBox_showBinary, SIGNAL(clicked(bool)), trackerWorker, SLOT(onShowBinary(bool)));
    connect(ui->checkBox_invert, SIGNAL(clicked(bool)), trackerWorker, SLOT(onInvertedBlob(bool)));
    connect(resizeHelper, SIGNAL(itemChanged(QGraphicsItem*)),this, SLOT(onGraphicsItemChanged(QGraphicsItem*)));
    connect(this, SIGNAL(collectBackground()), trackerWorker, SLOT(onCollectBackground()));


    //Connect ResizeHelper - Positionmapper
    connect(resizeHelper, SIGNAL(mouseClickEvent(QPointF)), &positionMapper, SLOT(onMouseClickEvent(QPointF)));

    //Connection TrackingGui & worker  - PositionMapper
    connect(this, SIGNAL(enableAutoTracking(bool)), &positionMapper, SLOT(onEnableAutoTracking(bool)));
    connect(trackerWorker, SIGNAL(trackingResult(std::vector<double>)), &positionMapper, SIGNAL(trackingResult(std::vector<double>)));
    connect(trackerWorker, SIGNAL(socketTrackingResult(std::vector<double>)), &positionMapper, SIGNAL(socketTrackingResult(std::vector<double>)));
    connect(&positionMapper, SIGNAL(moveStepperMotor(int, int)), &canInterface, SLOT(onMoveStepperMotor(int, int)));
    connect(&positionMapper, SIGNAL(changeMotorSpeed(int, int)), &canInterface, SLOT(onChangeMotorSpeed(int, int)));
    connect(&positionMapper, SIGNAL(cameraArrayPositionChanged(QRect)), trackerWorker, SLOT(onCameraPositionChanged(QRect)));


    //CAN interface & widget connections
    connect(ui->canWidget, SIGNAL(canSignal(QString,quint32,QByteArray)), &canInterface, SLOT(onCanSignal(QString,quint32,QByteArray)));
    connect(&canInterface, SIGNAL(sendMessage(qint32,QByteArray)), ui->canWidget, SLOT(sendMessage(qint32,QByteArray)));
    connect(this, SIGNAL(initializeStepperMotors()), &canInterface, SLOT(onInitializeStepperMotors()));
    connect(this, SIGNAL(moveStepperMotor(int,int)), &canInterface, SLOT(onMoveStepperMotor(int,int)));
    connect(this, SIGNAL(changeMotorSpeed(int,int)), &canInterface, SLOT(onChangeMotorSpeed(int,int)));

    connect(this, SIGNAL(cameraMatrixChanged(QVector<bool>)), &canInterface, SLOT(onCameraMatrixChanged(QVector<bool>)));
    connect(ui->pushButton_stopCameras, SIGNAL(clicked()), &canInterface, SLOT(onStopCameraMatrix()));
    connect(ui->pushButton_resumeCameras, SIGNAL(clicked()), &canInterface, SLOT(onResumeCameraMatrix()));
    emit(changeNumberOfHotAreas(hotAreas));

    //In order to write trackingresult to logfile things from caninterface need to be forwarded to worker.
    connect(&canInterface, SIGNAL(trackingResult(std::vector<double>)), trackerWorker, SIGNAL(trackingResult(std::vector<double>)));
    connect(&canInterface, SIGNAL(trackingMessage(std::string)), trackerWorker, SIGNAL(trackingMessage(std::vector<double>)));
    connect(this, SIGNAL(trackingMessage(std::string)), trackerWorker, SIGNAL(trackingMessage(std::string)));

    ui->horizontalSlider_xPosition->setMaximum(7500000);
    ui->horizontalSlider_yPosition->setMaximum(5900000);
    ui->horizontalSlider_speed->setMinimum(1000);
    ui->horizontalSlider_speed->setMaximum(100000);

    ui->lineEdit_PupilMinArea->setText(QString::number(1500));
    ui->lineEdit_PupilMaxArea->setText(QString::number(4000));
    //ui->horizontalSlider_threshold->setValue(70);
    ui->horizontalSlider_threshold->setValue(16);

    QTimer::singleShot(1000, this, SIGNAL(initializeStepperMotors()));

}

TrackerPlugin_MultiCamTrigger::~TrackerPlugin_MultiCamTrigger()
{
    canInterface.disableStepperMotor(0);
    canInterface.disableStepperMotor(1);

    /*Clean up Memory*/
    for(QPen* pen : rectPens)
        delete pen;

    delete pixmapItem;
    delete trackerWorker;
    delete ui;
}

QObject *TrackerPlugin_MultiCamTrigger::worker()
{
    return trackerWorker;
}

void TrackerPlugin_MultiCamTrigger::initializeUI(QLayout *layout, QGraphicsScene *cameraScene, QSize cameraResolution)
{
    /*Keep rectangles unchanged if the resolution didn't change*/
    if(this->cameraResolution != cameraResolution){
        this->cameraResolution = cameraResolution;

        layout->addWidget(this);
        this->cameraScene = cameraScene;

        /*Make objects resizeable and moveable*/
        resizeHelper->boundsRect = roi;
        this->cameraScene->addItem(resizeHelper);
        connect(this->cameraScene, SIGNAL(selectionChanged()), resizeHelper, SLOT(selectionChanged()));

        QRect cameraRect = QRect(0, 0, cameraResolution.width(), cameraResolution.height());

        for(int i=0;i<rectItems.size();i++){
            this->cameraScene->addItem(rectItems.at(i));
        }

        for(int i=0;i<hotAreaRectItems.size();i++){
            this->cameraScene->addItem(hotAreaRectItems.at(i));
        }

        /*Resize rectangles according to the correct size*/
        if(cameraResolution.width()>cameraResolution.height()){
            this->maxRectWidth = cameraResolution.height()/2;
        }else{
            this->maxRectWidth = cameraResolution.width()/2;
        }
        for(int i=0;i<rectItems.size();i++){
            if(!cameraRect.contains(roi)){
                roi = cameraRect;
            }
            rectItems.at(i)->setRect(roi);
            emit(thresholdChanged(i, uiSliders.at(i)->value()));
            emit(roiChanged(i, rectItems.at(i)->rect().toRect()));
        }
        for(int i=0;i<hotAreaRectItems.size();i++){
            if(!cameraRect.contains(hotAreas.at(i))){
                hotAreas[i] = cameraRect;
            }
            hotAreaRectItems.at(i)->setRect(hotAreas.at(i));
            emit(hotAreaChanged(i, hotAreaRectItems.at(i)->rect().toRect()));
        }
    }
}

void TrackerPlugin_MultiCamTrigger::saveSettings()
{
    QSettings settings(settingsFileName, QSettings::IniFormat );
    settings.beginGroup("cameras");
    settings.beginWriteArray("hotAreas");
    for(int i=0; i<hotAreas.size(); i++){
      settings.setArrayIndex(i);
      settings.setValue("roi", hotAreas.at(i));
    }
    settings.endArray();
    settings.setValue("roi", roi);

    settings.endGroup();
}

void TrackerPlugin_MultiCamTrigger::loadSettings()
{
    QSettings settings(settingsFileName, QSettings::IniFormat);
    settings.beginGroup("cameras");
    int size = settings.beginReadArray("hotAreas");
    hotAreas.resize(size);

    for(int i=0;i<size;i++){
      settings.setArrayIndex(i);
      hotAreas[i] = settings.value("roi").toRect();
    }
    settings.endArray();
    roi = settings.value("roi").toRect();
    settings.endGroup();

    //write stuff to logfile here?
    std::string message;
    emit(trackingMessage(message));
}

void TrackerPlugin_MultiCamTrigger::onThreshSliderMoved(int index)
{
    float value = (uiSliders.at(index)->value());
    uiLabels.at(index)->setText(QString("Threshold: ").append(QString::number(int(value))));
    emit(thresholdChanged(index, value));
}

void TrackerPlugin_MultiCamTrigger::onGraphicsItemChanged(QGraphicsItem *item)
{
    /*Determine which rectangle was changed (ROIS)*/
    int rectIdx = -1;
    QRectF rect(0,0,cameraResolution.width(),cameraResolution.height());
    for(int i = 0; i<rectItems.size(); i++){
        if(dynamic_cast<QGraphicsRectItem*>(item) == rectItems.at(i)){
            if (!rect.contains(rectItems.at(i)->rect())){
                return;
            }
            rectIdx = i;
        }
    }

    /*If the resized object is a ROI rectangle created by this plugin*/
    /*Send message*/
    if(rectIdx >= 0){
        emit(roiChanged(rectIdx, rectItems.at(rectIdx)->rect().toRect()));
    }else{
        /*Determine which rectangle was changed (hot areas)*/
        int hotAreaRectIdx = -1;
        /*Determine which rectangle was changed*/
        for(int i = 0; i<hotAreaRectItems.size(); i++){
            if(dynamic_cast<QGraphicsRectItem*>(item) == hotAreaRectItems.at(i)){
                hotAreaRectIdx = i;
            }
        }

        /*If the resized object is a ROI rectangle created by this plugin*/
        /*Send message*/
        if(hotAreaRectIdx >= 0){
            emit(hotAreaChanged(hotAreaRectIdx, hotAreaRectItems.at(hotAreaRectIdx)->rect().toRect()));
        }
    }
}

void TrackerPlugin_MultiCamTrigger::onAreaConstraintsEditingFinished(int index)
{
    emit(areaContraintsChanged(index, uiLineEdits.at(index)->text().toInt()));
}

void TrackerPlugin_MultiCamTrigger::on_horizontalSlider_xPosition_sliderReleased()
{
    int value = ui->horizontalSlider_xPosition->value();
    emit(moveStepperMotor(0, value));
}

void TrackerPlugin_MultiCamTrigger::on_horizontalSlider_yPosition_sliderReleased()
{
    int value = ui->horizontalSlider_yPosition->value();
    emit(moveStepperMotor(1, value));
}

void TrackerPlugin_MultiCamTrigger::on_pushButton_triggerCameras_clicked()
{
    QVector<bool>cameraMatrix(32, manualTriggerToggle);
    emit(cameraMatrixChanged(cameraMatrix));
    manualTriggerToggle = !manualTriggerToggle;
}

void TrackerPlugin_MultiCamTrigger::on_checkBox_showCanDetails_clicked(bool checked)
{
    ui->canWidget->setVisible(checked);
}


void TrackerPlugin_MultiCamTrigger::on_horizontalSlider_speed_sliderReleased()
{
    int value = ui->horizontalSlider_speed->value();
    emit(changeMotorSpeed(0, value));
    emit(changeMotorSpeed(1, value));
}


void TrackerPlugin_MultiCamTrigger::on_pushButton_collectBackground_clicked()
{
    emit(collectBackground());
}

void TrackerPlugin_MultiCamTrigger::on_checkBox_autoTracking_clicked(bool checked)
{
    emit(enableAutoTracking(checked));
}
