#ifndef POSITIONMAPPER_H
#define POSITIONMAPPER_H

#include <QObject>
#include <QDebug>
#include <QRect>
#include <opencv2/core/core.hpp>
#include <QPointF>
enum TrackingSource {camera, socket};

class PositionMapper : public QObject
{
    Q_OBJECT
public:
    explicit PositionMapper(QObject *parent = nullptr);

private:
    bool isAutoTracking = false;
    double lastBlobX = 0;
    double lastBlobY = 0;

    int motor0Max = 7500000;
    int motor1Max = 5900000;
    QVector<double> pastResults;
    int ringSize = 10;

signals:
    void moveStepperMotor(int motorIndex, int position);
    void changeMotorSpeed(int motorIndex, int speed);
    void cameraArrayPositionChanged(QRect cameraPosition);

    //forwarded from worker
    void trackingResult(std::vector<double> data); //The results are stored in a regular vector for easier communication with non-Qt C++ programs (e.g. through socket)
    void socketTrackingResult(std::vector<double> data); //The results are stored in a regular vector for easier communication with non-Qt C++ programs (e.g. through socket)

public slots:
    void onEnableAutoTracking(bool isEnabled);
    void onMouseClickEvent(QPointF pos);


private slots:
    void onTrackingResult(QVector<double> result, TrackingSource);
};

#endif // POSITIONMAPPER_H
